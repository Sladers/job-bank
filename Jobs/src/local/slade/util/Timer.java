package local.slade.util;

public class Timer {

	public static String format(long time) {
		StringBuilder sb = new StringBuilder();

		if (time < 1000) {
			sb.append(time);
			sb.append("ms");
		} else {
			// total seconds
			time /= 1000;
			long seconds = time % 60;

			// total minutes
			time /= 60;
			long minutes = time % 60;

			// total hours
			time /= 60;
			long hours = time % 24;

			// total days
			time /= 24;
			long days = time;

			if (days > 0) {
				sb.append(days);
				sb.append(":");
			}
			if (hours < 10) {
				sb.append("0");
			}
			sb.append(hours);
			sb.append(":");
			if (minutes < 10) {
				sb.append("0");
			}
			sb.append(minutes);
			sb.append(":");
			if (seconds < 10) {
				sb.append("0");
			}
			sb.append(seconds);
		}

		return sb.toString();
	}

	private final long duration;

	private long start = 0;

	public Timer(long duration) {
		this.duration = duration;
		reset();
	}

	public boolean isNotUp() {
		return !isUp();
	}

	public boolean isUp() {
		return timeElapsed() > duration;
	}

	public boolean isUpThenReset() {
		if (isUp()) {
			reset();
			return true;
		}
		return false;
	}

	public void reset() {
		start = System.currentTimeMillis();
	}

	public long timeElapsed() {
		return System.currentTimeMillis() - start;
	}

	public long timeRemaining() {
		return start + duration - System.currentTimeMillis();
	}

	@Override
	public String toString() {
		return "Timer[Duration: " + duration + "]";
	}

	public String toStringTimeElapsed() {
		return format(timeElapsed());
	}

	public String toStringTimeRemaining() {
		return format(timeRemaining());
	}

	public static void main(String[] args) throws InterruptedException {
		Timer t = new Timer(1000 * 60 * 72);

		while (t.isNotUp()) {
			System.out.println(t.toStringTimeElapsed() + ", " + t.toStringTimeRemaining());
			Thread.sleep(10000);
		}
	}

	public long getDuration() {
		return duration;
	}
}
