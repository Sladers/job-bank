package local.slade.application;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;
import local.slade.structs.Employer;
import local.slade.structs.PhoneNumber;

/**
 * Controller class for the EmployerView
 */
public class EmployerViewController {

	private Employer e = null;

	/**
	 * Sets the Employer to be edited in the View controller by this Controller
	 * 
	 * @param e
	 */
	public void setEmployer3(Employer e) {
		this.e = e;
		name.setText(e.getName());
		address.setText(e.getAddress());
		phoneNumber.getItems().setAll(e.getPhoneNumber());
		if (!phoneNumber.getItems().contains(null)) {
			phoneNumber.getItems().add(null);
		}
		websites.setText(e.getWebsite());
	}

	@FXML
	private URL location;

	@FXML
	private TextField name;

	@FXML
	private TextField address;

	@FXML
	private ListView<PhoneNumber> phoneNumber;

	@FXML
	private TextField websites;

	/**
	 * Saves the Employer being edited in the View controller by this Controller
	 */
	@FXML
	public void save() {
		if (JobsMain.confirmationDialog("save")) {
			e.setName(name.getText());
			e.setAddress(address.getText());
			List<PhoneNumber> numbers = new ArrayList<>(phoneNumber.getItems());
			while (numbers.contains(null)) {
				numbers.remove(null);
			}
			e.setPhoneNumber(numbers);
			e.setWebsite(websites.getText());
		}
	}

	@FXML
	void initialize() {

		phoneNumber.setEditable(true);
		phoneNumber.setCellFactory(
				(ListView<PhoneNumber> param) -> new TextFieldListCell<PhoneNumber>(new StringConverter<PhoneNumber>() {

					@Override
					public PhoneNumber fromString(String string) {
						return string != null ? new PhoneNumber(string) : null;
					}

					@Override
					public String toString(PhoneNumber pn) {
						return pn != null ? pn.toString() : null;
					}

				}));

		phoneNumber.setOnEditCommit(event -> {
			System.out.println("onEditCommit");
			System.out.println("Source: " + event.getSource());
			PhoneNumber selected = phoneNumber.getSelectionModel().getSelectedItem();
			System.out.println("Selected: " + selected);
			System.out.println("New Value: " + event.getNewValue());
			System.out.println("Index: " + event.getIndex());
			List<PhoneNumber> data = phoneNumber.getItems();
			data.remove(selected);
			data.add(event.getIndex(), event.getNewValue());
		});
	}
}
