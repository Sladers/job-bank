package local.slade.application;

import javafx.application.Platform;
import javafx.concurrent.Task;
import local.slade.util.Timer;

/**
 * 
 * Task<?> class that repeatedly updates the job list at a set frequency
 */
public class AutoUpdate extends Task<Void> {

	private JobListController controller;

	public AutoUpdate(JobListController controller) {
		this.controller = controller;
	}

	// initial time is set to 3 seconds and then increased in the main loop.
	private Timer timer = new Timer(1000 * 3);

	private int updates = 0;

	private long time = -1;

	@Override
	protected Void call() throws InterruptedException {

		while (!isCancelled()) {
			Thread.sleep(100);

			// check if a long time has passed since the last loop, because of
			// the comptuer going to sleep for example
			if (time != -1 && System.currentTimeMillis() - time > 10000) {
				System.out.println("Looks like computer just woke up");
				timer = new Timer(1000 * 10);
			}

			// keep track of the time it takes to do a loop
			time = System.currentTimeMillis();

			// perform an update whenever the timer is up
			if (timer.isUpThenReset()) {

				// if the timer is very short, make it longer. The timer is
				// short upon initialization
				if (timer.getDuration() <= 1000 * 60 * 10) {
					timer = new Timer(1000 * 60 * 60 * 4);
				}
				System.out.println("AutoUpdate timer is up");
				updates++;
				System.out.println("Updating list automatically");

				// update call can be made on FXApplication thread because it
				// spawns additional threads to do the scraping. Call MUST be
				// done on FXApplication thread because Nodes are created
				Platform.runLater(() -> {
					controller.updateList(true);
					System.out.println("Done updating automatically or done initiating update");
				});
			}
			// the message will be the timer time remaining
			updateMessage(timer.toStringTimeRemaining());
			// the title will be the number of updates
			updateTitle(Integer.toString(updates));
		}

		if (isCancelled()) {
			System.out.println("AutoUpdate task cancelled");
		}

		// return type is Void
		return null;
	}

}
