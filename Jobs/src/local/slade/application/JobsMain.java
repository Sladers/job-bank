package local.slade.application;

import java.net.URL;
import java.util.Optional;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import local.slade.db.CollectionDB;
import local.slade.db.DbConnection;

/**
 * Main class
 */
public class JobsMain extends Application {

	// private final DbConnection db = new SqliteDB(SqliteDB.DEFAULT);
	private final DbConnection db = new CollectionDB();

	@Override
	public void start(Stage primaryStage) {
		db.load();
		primaryStage.setTitle("Job List");
		try {
			URL url = getClass().getResource("JobList.fxml");
			System.out.println(url);
			FXMLLoader loader = new FXMLLoader(url);
			Parent root = loader.load();

			JobListController jlc = loader.getController();
			jlc.setDB(db);

			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();

			primaryStage.setOnHiding((WindowEvent evt) -> {
				System.out.println("Hiding");
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Creates and shows a confirmation dialog containing "Are you sure you want
	 * to *message*". Convenience method used in some of the controllers
	 * 
	 * @param message
	 * @return
	 */
	public static boolean confirmationDialog(String message) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to " + message + "?");
		Optional<ButtonType> result = alert.showAndWait();

		return result.get() == ButtonType.OK;
	}

}
