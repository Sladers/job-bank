package local.slade.application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import local.slade.structs.Job;

/**
 * 
 * Controller class for the JobView, a view of the properties of a single Job.
 */
public class JobViewController {

	/**
	 * Saves the job viewed by this JobView
	 */
	@FXML
	private void save() {
		if (JobsMain.confirmationDialog("save")) {
			System.out.println("Save Job");
			job.setId(Integer.parseInt(id.getText()));
			job.getEmployer().setName(employer.getText());
			job.setDatePosted(postDate.getValue());
			job.setLastSeen(lastSeen.getValue());
		}
	}

	private Job job = null;

	/**
	 * Sets the job to be viewed in this View
	 * 
	 * @param j
	 */
	public void setJob(Job j) {
		this.job = j;
		id.setText(Integer.toString(j.getId()));
		employer.setText(j.getEmployer() != null ? j.getEmployer().toString() : "null");
		postDate.setValue(j.getDatePosted());
		lastSeen.setValue(j.getLastSeen());
		textFlow.getChildren().add(new Text(j.getAdContent()));
	}

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private VBox vbox;

	@FXML
	private MenuBar menuBar;

	@FXML
	private Menu fileMenu;

	@FXML
	private GridPane gridpane;

	@FXML
	private TextField id;

	@FXML
	private TextField employer;

	@FXML
	private DatePicker lastSeen;

	@FXML
	private DatePicker postDate;

	@FXML
	private TextFlow textFlow;

	@FXML
	private void initialize() {
		assert vbox != null : "fx:id=\"vbox\" was not injected: check your FXML file 'JobView.fxml'.";
		assert menuBar != null : "fx:id=\"menuBar\" was not injected: check your FXML file 'JobView.fxml'.";
		assert fileMenu != null : "fx:id=\"fileMenu\" was not injected: check your FXML file 'JobView.fxml'.";
		assert gridpane != null : "fx:id=\"gridpane\" was not injected: check your FXML file 'JobView.fxml'.";
		assert id != null : "fx:id=\"id\" was not injected: check your FXML file 'JobView.fxml'.";
		assert employer != null : "fx:id=\"employer\" was not injected: check your FXML file 'JobView.fxml'.";
		assert lastSeen != null : "fx:id=\"lastSeen\" was not injected: check your FXML file 'JobView.fxml'.";
		assert postDate != null : "fx:id=\"postDate\" was not injected: check your FXML file 'JobView.fxml'.";
		assert textFlow != null : "fx:id=\"textFlow\" was not injected: check your FXML file 'JobView.fxml'.";

	}
}