package local.slade.application;

import java.io.IOException;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TitledPane;

/**
 * 
 * A TitledPane containing a progress bar and some labels. The progress bar and
 * labels are bound to a Task by calling setTask
 */
public class ProgressWindowRoot extends TitledPane {

	/**
	 * Constructions the ProgressWindowRoot backed by fx:root
	 */
	public ProgressWindowRoot() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("ProgressWindowRoot.fxml"));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@FXML
	private Label title;

	@FXML
	private Label message;

	@FXML
	private ProgressBar progressbar;

	public DoubleProperty progressBarProperty() {
		return progressbar.progressProperty();
	}

	public StringProperty messageProperty() {
		return message.textProperty();
	}

	/**
	 * Binds the given task to the progressbar, and message label
	 * 
	 * @param task
	 */
	public void setTask(Task<?> task) {
		message.textProperty().bind(task.messageProperty());
		progressbar.progressProperty().bind(task.progressProperty());
	}

}
