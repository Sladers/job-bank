package local.slade.application;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * Controller class for the ProgressWindowRoot
 */
public class ProgressWindowController {

	@FXML
	private Label title;

	@FXML
	private Label message;

	@FXML
	private ProgressBar progressbar;

	public void setTask(Task<?> task) {
		title.textProperty().bind(task.titleProperty());
		message.textProperty().bind(task.messageProperty());
		progressbar.progressProperty().bind(task.progressProperty());
	}

}
