package local.slade.application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import local.slade.db.DbConnection;
import local.slade.structs.Employer;
import local.slade.structs.PhoneNumber;

/**
 * 
 * Controller class for the Employer List.
 */
public class EmployerListController {

	private DbConnection db = null;
	private ObservableList<Employer> data = null;

	/**
	 * To be called after initializion
	 * 
	 * @param db
	 */
	public void setDB(DbConnection db) {
		this.db = db;
		data = (ObservableList<Employer>) db.getAllEmployers();
		table.setItems(data);
	}

	@FXML
	private ResourceBundle resources;

	@FXML
	private Button save;

	@FXML
	private Button delete;

	@FXML
	private TableView<Employer> table;

	@FXML
	private TableColumn<Employer, String> nameColumn;

	@FXML
	private TableColumn<Employer, String> addressColumn;

	@FXML
	private TableColumn<Employer, String> websiteColumn;

	@FXML
	private TableColumn<Employer, PhoneNumber> phoneColumn;

	@FXML
	private Label employerCount;

	@FXML
	public void edit() {
		System.out.println("Edit employer");

		Employer selected = selected();
		if (selected != null) {
			try {
				URL url = getClass().getResource("EmployerView.fxml");
				FXMLLoader loader = new FXMLLoader(url);
				Parent root = loader.load();

				EmployerViewController evc = loader.getController();
				evc.setEmployer3(selected);

				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.setScene(scene);

				stage.show();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No employer selected");
		}
	}

	private Employer selected() {
		return table.getSelectionModel().getSelectedItem();
	}

	/**
	 * Delete the selected employer
	 */
	@FXML
	public void delete() {

		if (JobsMain.confirmationDialog("delete this employer?")) {
			System.out.println("delete employer");
			Employer selected = selected();
			db.remove(selected);
			data.remove(selected);
			db.getAllJobs().removeAll(db.getJobs(selected));
		}
	}

	/**
	 * Save the DB
	 */
	@FXML
	public void save() {
		if (JobsMain.confirmationDialog("save")) {
			System.out.println("Save employer list");
			db.save();
		}
	}

	@FXML
	private void initialize() {
		assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert delete != null : "fx:id=\"delete\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert nameColumn != null : "fx:id=\"nameColumn\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert addressColumn != null : "fx:id=\"addressColumn\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert websiteColumn != null : "fx:id=\"websiteColumn\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert phoneColumn != null : "fx:id=\"phoneColumn\" was not injected: check your FXML file 'Employer3View.fxml'.";
		assert employerCount != null : "fx:id=\"employerCount\" was not injected: check your FXML file 'Employer3View.fxml'.";

		nameColumn.setCellValueFactory(new PropertyValueFactory<Employer, String>("name"));
		addressColumn.setCellValueFactory(new PropertyValueFactory<Employer, String>("address"));
		websiteColumn.setCellValueFactory(new PropertyValueFactory<Employer, String>("website"));
		phoneColumn.setCellValueFactory(new PropertyValueFactory<Employer, PhoneNumber>("phoneNumber"));

		nameColumn.setEditable(true);
		nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		nameColumn.setOnEditCommit((CellEditEvent<Employer, String> event) -> {
			Employer j = event.getRowValue();
			j.setName(event.getNewValue());
		});

	}

}
