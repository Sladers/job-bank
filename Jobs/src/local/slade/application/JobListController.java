package local.slade.application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import local.slade.db.DbConnection;
import local.slade.scraping.ConfederationCollegeScraper;
import local.slade.scraping.FFTimesScraper;
import local.slade.scraping.IndeedScraper;
import local.slade.scraping.Scraper;
import local.slade.structs.Employer;
import local.slade.structs.Job;

/**
 * 
 * Controller class for the JobList view
 */
public class JobListController {

	private DbConnection db = null;

	/**
	 * Saves the DB if the user confirms they want to
	 */
	@FXML
	public void save() {
		save(false);
	}

	/**
	 * Saves the DB.
	 * 
	 * @param forced
	 *            whether or not to force the save or to ask the user to confirm
	 *            saving
	 */
	private void save(boolean forced) {
		System.out.println("Save Job List");
		if (forced || JobsMain.confirmationDialog("save")) {
			if (!forced) {
				for (Employer e : new ArrayList<>(db.getAllEmployers())) {
					if (e.isEmpty()) {
						if (db.getJobs(e).size() == 0) {
							db.remove(e);
						}
					}
				}
			}
			db.save();
		}
	}

	/**
	 * Configures the given file chooser to only accept json files, start in the
	 * user's desktop, and gives it the given title.
	 * 
	 * @param chooser
	 * @param title
	 */
	private static void configureFileChooser(FileChooser chooser, String title) {
		chooser.setTitle(title);
		ExtensionFilter filter = new ExtensionFilter("JavaScript Object Notion", "*.json");
		chooser.getExtensionFilters().add(filter);
		chooser.setInitialDirectory(new File(System.getProperty("user.home") + File.separator + "Desktop"));
	}

	/**
	 * Exports the Job list to a user selected json file
	 */
	@FXML
	private void exportJSON() {
		System.out.println("Exporting as JSON using JackSON");
		FileChooser fc = new FileChooser();
		configureFileChooser(fc, "Choose Save File");

		System.out.println("Showing File Chooser");
		File f = fc.showSaveDialog(table.getScene().getWindow());
		if (f != null) {
			for (Job j : db.getAllJobs()) {
				j.setAdContent(j.getAdContent());
			}
			System.out.println("Got a save file");
			ObjectMapper mapper = getJsonMapper();

			try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8)) {
				String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(db.getAllJobs());
				out.write(jsonString);
				System.out.println("Done writing job list file");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns a correctly configured ObjectMapper. Used for consistency
	 * 
	 * @return
	 */
	private static ObjectMapper getJsonMapper() {
		ObjectMapper mapper = new ObjectMapper();

		mapper.findAndRegisterModules();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		return mapper;
	}

	/**
	 * Imports a user selected json file
	 */
	@FXML
	private void importJSON() {
		FileChooser fc = new FileChooser();
		configureFileChooser(fc, "Choose File to Import");

		File f = fc.showOpenDialog(table.getScene().getWindow());
		if (f != null) {
			System.out.println("Got file to import from");
			ObjectMapper mapper = getJsonMapper();
			try {
				System.out.println("Attempting to read arraylist");
				ArrayList<Job> list = mapper.readValue(f, new TypeReference<ArrayList<Job>>() {
				});
				System.out.println("List contents: ");
				for (Object o : list) {
					System.out.println(o);
				}
				System.out.println("Jobs imported: " + list.size());
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * returns the currectly selected Job, if any;
	 * 
	 * @return
	 */
	private Job selected() {

		return table.getSelectionModel().getSelectedItem();
	}

	/**
	 * Opens the selected job in a standalone JobView so that it can be edited.
	 */
	@FXML
	private void edit() {
		Job j = selected();

		if (j != null) {
			try {
				URL url = getClass().getResource("JobView.fxml");
				FXMLLoader loader = new FXMLLoader(url);
				Parent root = loader.load();

				JobViewController jvc = loader.getController();

				jvc.setJob(j);

				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.setScene(scene);
				stage.show();

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			}
		}
	}

	@FXML
	private void editEmployers() {
		System.out.println("Edit employers");

		try {
			URL url = getClass().getResource("EmployerList.fxml");
			FXMLLoader loader = new FXMLLoader(url);
			Parent root = loader.load();

			EmployerListController evc = loader.getController();

			evc.setDB(db);

			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
			stage.setOnHiding(e -> setJobList(db.getAllJobs()));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}

	}

	/**
	 * Hides the currently selected Job
	 */
	@FXML
	private void hideJob() {
		if (JobsMain.confirmationDialog("hide the selected entry")) {
			Job j = selected();
			j.setHidden(true);
			table.getItems().remove(j);
		}
	}

	/**
	 * Deletes the currently selected Job
	 */
	@FXML
	private void delete() {
		if (JobsMain.confirmationDialog("delete the selected entry")) {
			Job j = selected();
			db.remove(j);
			table.getItems().remove(j);
		}
	}

	/**
	 * Updates the job list
	 */
	@FXML
	private void updateList() {
		updateList(false);
	}

	/**
	 * Updates the job list. Can automatically close the progress windows if
	 * updating automatically
	 * 
	 * @param closeProgressWindow
	 *            whether or not to automatically close the progress window
	 */
	public void updateList(boolean closeProgressWindow) {
		System.out.println("Updating list threadularily");

		/*
		 * List of all scrapers to use. Can add scrapers here if we wish to
		 * scrape additional websites
		 */
		Scraper[] scrapers = { //
				new FFTimesScraper(db), //
				new IndeedScraper(db), //
				new ConfederationCollegeScraper(db),//
		};

		// use a Property so that we can access the wrapped value in lambda
		final SimpleIntegerProperty counter = new SimpleIntegerProperty(0);

		VBox vbox = new VBox();

		for (Scraper scraper : scrapers) {
			System.out.println("Adding scraper: " + scraper.title());
			ProgressWindowRoot c = new ProgressWindowRoot();
			c.setText(scraper.title());
			c.setTask(scraper);
			vbox.getChildren().add(c);
		}

		Scene scene = new Scene(vbox);
		Stage stage = new Stage();
		stage.setScene(scene);

		System.out.println("Initializing scrapers");
		for (Scraper scraper : scrapers) {
			scraper.setOnSucceeded(event -> {
				System.out.println("Task Success");
				List<Job> result = (List<Job>) event.getSource().getValue();
				System.out.println("New Jobs found: " + result.size());
				// for (Job j : result) {
				// System.out.println(j);
				// }
				db.addAll(result);
				db.updateJobStatus();
				setJobList(db.getAllJobs());
				jobCountProperty.set(db.countJobs());
				sort();
				counter.set(counter.get() + 1);
				if (closeProgressWindow && counter.get() == scrapers.length) {
					System.out.println("Closing Progress Window automatically");
					stage.close();
				}
				if (counter.get() == scrapers.length) {
					save(true);
				}
			});
			Thread t = new Thread(scraper);
			t.setDaemon(true);
			System.out.println("Starting scraper: " + scraper.title());
			t.start();
		}

		stage.show();
		System.out.println("End of updateList(" + closeProgressWindow + ")");
	}

	/**
	 * Sets the db to be used by this JobList
	 * 
	 * @param db
	 */
	public void setDB(DbConnection db) {
		this.db = db;

		ObservableList<Job> data = FXCollections.observableList(db.getAllJobs());
		Collections.sort(data);

		setJobList(data);

		jobCount.textProperty().bind(Bindings.concat("Job Count: ", jobCountProperty));
		freshCount.textProperty().bind(Bindings.concat("Fresh: ", db.freshJobsProperty()));
		activeCount.textProperty().bind(Bindings.concat("Active: ", db.activeJobsProperty()));
	}

	private IntegerProperty jobCountProperty = new SimpleIntegerProperty();

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TableView<Job> table;

	@FXML
	private TableColumn<Job, Integer> idColumn;

	@FXML
	private TableColumn<Job, Employer> employerColumn;

	@FXML
	private TableColumn<Job, String> contentColumn;

	@FXML
	private TableColumn<Job, LocalDate> datePostedColumn;

	@FXML
	private TableColumn<Job, LocalDate> lastSeenColumn;

	@FXML
	private Label jobCount;

	@FXML
	private ImageView imageView;

	@FXML
	private Label freshCount;

	@FXML
	private Label activeCount;

	@FXML
	private TextArea preview;

	@FXML
	private WebView webview;

	@FXML
	private Label updates;

	@FXML
	private Label nextUpdate;

	/**
	 * Closes the window in the user confirms
	 */
	@FXML
	public void closeWindow() {

		System.out.println("Close window");
		if (JobsMain.confirmationDialog("exit")) {
			Stage stage = (Stage) table.getScene().getWindow();

			stage.close();
		}

	}

	/**
	 * Sets the list of jobs to be used in this view
	 * 
	 * @param jobs
	 */
	public void setJobList(List<Job> jobs) {
		List<Job> unhidden = new ArrayList<>();
		jobs.forEach((Job j) -> {
			if (!j.isHidden()) {
				unhidden.add(j);
			}
		});
		table.getItems().setAll(unhidden);
	}

	/**
	 * Track mouse clicks to easily edit Jobs in the list
	 * 
	 * @param event
	 */
	@FXML
	public void mouseClicked(MouseEvent event) {
		double x = event.getSceneX();
		double y = event.getSceneY();

		System.out.println("Mouse Clicked: (" + x + ", " + y + ") Count: " + event.getClickCount());
		if (event.getClickCount() == 2) {
			edit();
		}
	}

	/**
	 * Loads the user selected file
	 */
	@FXML
	private void load() {
		System.out.println("Load");
		FileChooser chooser = new FileChooser();

		File file = chooser.showOpenDialog(preview.getScene().getWindow());

		if (file != null) {
			// TODO load old DB from file. Depends on DB type.
		} else {
			System.out.println("Failed to select a file to load from");
		}
	}

	/**
	 * Returns the list back to default sort order.
	 */
	@FXML
	private void sort() {
		System.out.println("Sort");
		table.getSortOrder().clear();
		Collections.sort(table.getItems());
	}

	@FXML
	private void initialize() {
		preview.setVisible(false);
		preview.setWrapText(true);
		idColumn.setCellValueFactory(new PropertyValueFactory<Job, Integer>("id"));
		employerColumn.setCellValueFactory(new PropertyValueFactory<Job, Employer>("employer"));
		contentColumn.setCellValueFactory(new PropertyValueFactory<Job, String>("adContent"));
		datePostedColumn.setCellValueFactory(new PropertyValueFactory<Job, LocalDate>("datePosted"));
		lastSeenColumn.setCellValueFactory(new PropertyValueFactory<Job, LocalDate>("lastSeen"));

		table.setFixedCellSize(60);

		idColumn.setEditable(false);

		employerColumn.setEditable(true);
		Callback<TableColumn<Job, Employer>, TableCell<Job, Employer>> cb = TextFieldTableCell
				.<Job, Employer>forTableColumn(new StringConverter<Employer>() {

					@Override
					public Employer fromString(String value) {
						return new Employer(value);
					}

					@Override
					public String toString(Employer value) {
						if (value != null) {
							return value.toString();
						} else
							return null;
					}

				});
		employerColumn.setCellFactory(cb);
		employerColumn.setOnEditCommit((CellEditEvent<Job, Employer> event) -> {
			Job j = event.getRowValue();
			j.getEmployer().setName(event.getNewValue().getName());
		});
		contentColumn.setEditable(false);
		table.setEditable(true);

		table.getSelectionModel().selectedItemProperty().addListener((observable, oldSelections, newSelection) -> {
			if (newSelection != null) {
				String content = newSelection.getAdContent();
				if (newSelection.getImage() != null) {
					webview.setVisible(false);
					preview.setVisible(false);
					imageView.setVisible(true);
					previewPane.setVisible(true);
					String url = "file:images" + File.separator + newSelection.getImage();
					Image image = new Image(url);
					if (image.isError()) {
						System.out.println("Failed to load saved image from local url: " + url);
					}
					imageWidth.set(image.getWidth());
					imageHeight.set(image.getHeight());
					imageView.setImage(image);
				} else if (content.contains("<")) {
					webview.getEngine().loadContent(content);
					webview.setVisible(true);
					preview.setVisible(false);
					imageView.setVisible(false);
					previewPane.setVisible(false);
				} else {
					preview.setText(content);
					preview.setVisible(true);
					webview.setVisible(false);
					imageView.setVisible(false);
					previewPane.setVisible(false);
				}

			}
		});

		updater = new AutoUpdate(this);
		updates.textProperty().bind(updater.titleProperty());
		nextUpdate.textProperty().bind(updater.messageProperty());
		Thread t = new Thread(updater);
		t.setDaemon(true);
		t.start();

	}

	private DoubleProperty imageWidth = new SimpleDoubleProperty(40);
	private DoubleProperty imageHeight = new SimpleDoubleProperty(40);

	@FXML
	private ScrollPane previewPane;

	private AutoUpdate updater;

}
