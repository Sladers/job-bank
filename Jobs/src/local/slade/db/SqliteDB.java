package local.slade.db;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import local.slade.structs.Employer;
import local.slade.structs.Job;
import local.slade.structs.PhoneNumber;

/**
 * Implementation of an abstract DbConnection to a SqliteDB. Work in progress.
 * 
 */
public class SqliteDB extends DbConnection {

	public static final File DEFAULT = new File("jobsql.db");

	private final class EmployerTable {
		private static final String TITLE = "Employers";
		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String ADDRESS = "address";
		private static final String WEBSITE = "website";
	}

	private final class JobTable {
		private static final String TITLE = "JOBS";
		private static final String ID = "id";
		private static final String EMPLOYER = "Employer";
		private static final String DATE_POSTED = "datePosted";
		private static final String LAST_SEEN = "lastSeen";
		private static final String CONTENT = "content";
	}

	private final class PhoneNumberTable {
		private static final String TITLE = "PHONE_NUMBERS";
		private static final String PHONE_NUMBER = "phoneNumber";
		private static final String EMPLOYER = "employer";
	}

	private final File file;

	@SuppressWarnings("unused")
	private static void logResult(ResultSet result) {
		try {
			ResultSetMetaData data = result.getMetaData();

			for (int i = 1; i <= data.getColumnCount(); i++) {
				System.out.print(result.getObject(i));
				if (i < data.getColumnCount()) {
					System.out.print(", ");
				}
			}
			System.out.println();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// private static final Employer3 eon = new Employer3("Elections Ontario",
	// Arrays.asList(new PhoneNumber(8072746990L)),
	// "620 Church St", "electons.on.ca");
	// private static final Employer3 ffits = new Employer3("Fort Frances IT
	// Services",
	// Arrays.asList(new PhoneNumber(8072740094L), new PhoneNumber(2744848)),
	// "448 Third St E", "ffits.ca");
	// private static final Employer3 null1 = new Employer3(null,
	// Arrays.asList(new PhoneNumber(8675309)), null,
	// "stuff.com");
	// private static final Employer3 null2 = new Employer3(null,
	// Arrays.asList(new PhoneNumber(1234567)), null,
	// "farts.com");

	// private static void initial(File file) {
	// file.delete();
	// SqliteDB db = new SqliteDB(file);
	//
	// db.addEmployer(eon);
	// db.addEmployer(ffits);
	// db.addEmployer(null1);
	// db.addEmployer(null2);
	//
	// List<Employer3> employers = db.getAllEmployers();
	// System.out.println("Employers");
	// for (Employer3 e : employers) {
	// System.out.println(e.fields());
	// }
	//
	// Connection conn = db.getConnection();
	// try {
	// Statement statement = conn.createStatement();
	// String query = "SELECT * from " + EmployerTable.TITLE;
	// ResultSet rs = statement.executeQuery(query);
	// while (rs.next()) {
	// logResult(rs);
	// }
	// } catch (SQLException e1) {
	// e1.printStackTrace();
	// }
	// }

	public static void dump(File file) {

		DbConnection db = new SqliteDB(file);
		List<Employer> employers = db.getAllEmployers();
		for (Employer e : employers) {
			System.out.println(e.fields());
		}
		List<Job> jobs = db.getAllJobs();
		for (Job j : jobs) {
			System.out.println(j);
		}
	}

	public static void main(String[] args) throws SQLException {

		// dump(file);
		SqliteDB db = new SqliteDB(DEFAULT);
		CollectionDB cdb = new CollectionDB();

		cdb.load();

		System.out.println(db.getAllEmployers().size() + " =? " + db.countEmployers());
		System.out.println(cdb.getAllEmployers().size() + " =? " + cdb.countEmployers());

	}

	private Connection conn = null;

	@Override
	public void addAll(Collection<Job> e) {
		for (Job j : e) {
			addJob(j);
		}
	}

	@Override
	public void addEmployer(Employer employer) {
		// System.out.println("Add: " + employer.fields());
		String name = employer.getName();
		String address = employer.getAddress();
		String website = employer.getWebsite();

		Connection conn = getConnection();

		try {
			String query = "INSERT INTO " + EmployerTable.TITLE + " ("//
					+ EmployerTable.ID + "," + EmployerTable.NAME + ", " + EmployerTable.ADDRESS + ", "
					+ EmployerTable.WEBSITE + ")\n"//
					+ " VALUES (NULL, ?, ?, ?);";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, name);
			statement.setString(2, address);
			statement.setString(3, website);
			statement.executeUpdate();
			query = "SELECT last_insert_rowid()";
			statement = conn.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			int id = result.getInt(1);

			query = "INSERT OR REPLACE INTO " + PhoneNumberTable.TITLE + " ("//
					+ PhoneNumberTable.EMPLOYER + ", " + PhoneNumberTable.PHONE_NUMBER + ") "//
					+ " VALUES ( ?, ?);";
			statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			for (PhoneNumber pn : employer.getPhoneNumber()) {
				statement.setLong(2, pn.getNumber());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private int getId(Employer employer) throws SQLException {

		Connection conn = getConnection();
		StringBuilder query = new StringBuilder("SELECT ").append(EmployerTable.ID).append(" from ")
				.append(EmployerTable.TITLE);
		query = query.append(joinPhoneNumber());

		query = query.append(" WHERE ");
		StringBuilder sb = new StringBuilder(query.toString());
		List<Pair<String, Object>> list = new ArrayList<>();
		if (notNullNotEmpty(employer.getName())) {
			list.add(new Pair<String, Object>(EmployerTable.NAME, employer.getName()));
		}
		if (notNullNotEmpty(employer.getAddress())) {
			list.add(new Pair<String, Object>(EmployerTable.ADDRESS, employer.getAddress()));
		}
		if (notNullNotEmpty(employer.getWebsite())) {
			list.add(new Pair<String, Object>(EmployerTable.WEBSITE, employer.getWebsite()));
		}

		for (PhoneNumber pn : employer.getPhoneNumber()) {
			list.add(new Pair<String, Object>(PhoneNumberTable.PHONE_NUMBER, pn.getNumber()));
		}
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				// query.append("");
				query.append(list.get(i).getKey());
				sb.append(list.get(i).getKey());
				query.append(" = ?");
				sb.append(" = ");
				sb.append(list.get(i).getValue());
				if (i < list.size() - 1) {
					query.append(" OR ");
					sb.append(" OR ");
				}
			}
			System.out.println(query);
			System.out.println(sb);
			PreparedStatement statement = conn.prepareStatement(query.toString());

			for (int i = 0; i < list.size(); i++) {
				statement.setObject(i + 1, list.get(i).getValue());
			}
			ResultSet result = statement.executeQuery();

			if (result.next()) {
				return result.getInt(1);
			}
		}
		return -1;
	}

	@Override
	public void addJob(Job job) {
		Connection conn = getConnection();
		String query;
		// if (job.getId() == -1) {
		// query = "INSERT INTO " + JobTable.TITLE + "(" + JobTable.ID + ", " +
		// JobTable.EMPLOYER + ", "
		// + JobTable.DATE_POSTED + ", " + JobTable.LAST_SEEN + ", " +
		// JobTable.CONTENT + ") "
		// + "VALUES (NULL, ?, ?, ?, ?);";
		// } else {
		query = "INSERT OR REPLACE INTO " + JobTable.TITLE + " (" + JobTable.ID + ", " + JobTable.EMPLOYER + ", "
				+ JobTable.DATE_POSTED + ", " + JobTable.LAST_SEEN + ", " + JobTable.CONTENT + ") "
				+ "VALUES (?, ?, ?, ?, ?);";
		// }
		// System.out.println(query);
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			if (job.getId() == -1) {
				statement.setObject(1, null);
			} else {
				statement.setInt(1, job.getId());
			}
			statement.setInt(2, getId(job.getEmployer()));
			statement.setString(3, job.getDatePosted().toString());
			statement.setString(4, job.getLastSeen().toString());
			statement.setString(5, job.getAdContent());
			int result = statement.executeUpdate();
			System.out.println("Add job result: " + result);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int countEmployers() {
		Connection conn = getConnection();
		String query = "SELECT " + EmployerTable.ID + " FROM " + EmployerTable.TITLE;

		try {
			ResultSet result = conn.createStatement().executeQuery(query);
			int count = 0;
			while (result.next()) {
				count++;
			}
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

	@Override
	public int countJobs() {
		Connection conn = getConnection();
		String query = "SELECT from " + JobTable.TITLE + " *";

		try {
			ResultSet result = conn.createStatement().executeQuery(query);
			int count = 0;
			while (result.next()) {
				count++;
			}
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

	private void createDatabase() throws SQLException {
		Connection conn = getConnection();

		System.out.println("Creating table: " + EmployerTable.TITLE);
		String query = "CREATE TABLE IF NOT EXISTS " + EmployerTable.TITLE + "(\n"//
				+ EmployerTable.ID + " integer PRIMARY KEY AUTOINCREMENT,\n"//
				+ EmployerTable.NAME + " text,\n"//
				+ EmployerTable.ADDRESS + " text,\n"//
				+ EmployerTable.WEBSITE + " text\n"//
				+ ");";//
		// System.out.println(query);
		PreparedStatement statement = conn.prepareStatement(query);
		statement.execute();

		System.out.println("Creating table: " + JobTable.TITLE);
		query = "CREATE TABLE IF NOT EXISTS " + JobTable.TITLE + " (\n"//
				+ JobTable.ID + " integer PRIMARY KEY AUTOINCREMENT,\n"//
				+ JobTable.EMPLOYER + " integer SECONDARY KEY,\n"
				// TODO Foreign Key EmployersTable.name
				+ JobTable.DATE_POSTED + " text,\n"//
				+ JobTable.LAST_SEEN + " text,\n"//
				+ JobTable.CONTENT + " text);";//
		// System.out.println(query);
		statement = conn.prepareStatement(query);
		statement.execute();

		System.out.println("Creating table: " + PhoneNumberTable.TITLE);
		query = "CREATE TABLE IF NOT EXISTS " + PhoneNumberTable.TITLE + " (\n"//
				+ PhoneNumberTable.PHONE_NUMBER + " integer PRIMARY KEY,\n"//
				+ "employer integer SECONDARY KEY\n"
				// TODO foreign key EmployersTable.ID
				+ ");";//
		statement = conn.prepareStatement(query);
		// System.out.println(query);
		statement.execute();

	}

	@Override
	public ObservableList<Employer> getAllEmployers() {

		HashMap<Integer, Employer> map = new HashMap<>();

		Connection conn = getConnection();
		String query = "SELECT "//
				+ EmployerTable.ID + ", "//
				+ EmployerTable.NAME + ", "//
				+ EmployerTable.ADDRESS + ", "//
				+ EmployerTable.WEBSITE + ", " + PhoneNumberTable.TITLE + "." + PhoneNumberTable.PHONE_NUMBER + " as "
				+ PhoneNumberTable.PHONE_NUMBER//
				+ " FROM " + EmployerTable.TITLE//
				+ " INNER JOIN " + PhoneNumberTable.TITLE + " ON " + EmployerTable.TITLE + "." + EmployerTable.ID
				+ " = " + PhoneNumberTable.TITLE + "." + PhoneNumberTable.EMPLOYER//
				+ ";";

		StringBuilder sb = new StringBuilder("SELECT * FROM " + EmployerTable.TITLE).append(joinPhoneNumber());
		query = sb.toString();
		System.out.println(query);
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				// logResult(result);
				int id = result.getInt(EmployerTable.ID);
				Employer e = map.get(id);

				if (e == null) {
					e = new Employer();
					e.setName(result.getString(EmployerTable.NAME));
					e.setAddress(result.getString(EmployerTable.ADDRESS));
					e.setWebsite(result.getString(EmployerTable.WEBSITE));
					map.put(id, e);
				}
				long pn = result.getLong(PhoneNumberTable.PHONE_NUMBER);
				if (pn != 0) {
					e.addPhoneNumber(new PhoneNumber(pn));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return FXCollections.observableArrayList(map.values());
	}

	@Override
	public List<Job> getAllJobs() {
		List<Job> list = new ArrayList<Job>();
		String query = "SELECT * FROM " + JobTable.TITLE + ";";
		Connection conn = getConnection();
		try {
			ResultSet result = conn.createStatement().executeQuery(query);
			while (result.next()) {
				Job j = parseJob(result);

				list.add(j);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return list;
	}

	public List<PhoneNumber> getAllPhoneNumbers() {
		List<PhoneNumber> list = new ArrayList<PhoneNumber>();

		Connection conn = getConnection();
		String query = "SELECT * FROM " + PhoneNumberTable.TITLE + ";";
		Statement statement;
		try {
			statement = conn.createStatement();
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {

				long number = result.getLong(PhoneNumberTable.PHONE_NUMBER);

				// System.out.println("Next phone number: " + employer + ", " +
				// number);

				list.add(new PhoneNumber(number));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	private Connection getConnection() {
		if (conn == null) {
			try {
				if (!file.exists()) {
					file.createNewFile();
				}
				String url = "jdbc:sqlite:" + file.getAbsolutePath();
				conn = DriverManager.getConnection(url);
				createDatabase();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return conn;
	}

	private static boolean notNullNotEmpty(String s) {
		return s != null && !s.isEmpty();
	}

	private Employer getEmployer(int id) throws SQLException {
		Connection conn = getConnection();
		StringBuilder query = new StringBuilder("SELECT * FROM ").append(EmployerTable.TITLE).append(joinPhoneNumber())//
				.append(" WHERE ").append(EmployerTable.ID).append(" = ?");
		PreparedStatement statement = conn.prepareStatement(query.toString());
		statement.setInt(1, id);
		ResultSet result = statement.executeQuery();
		return getEmployer(result);
	}

	private static Employer getEmployer(ResultSet result) throws SQLException {
		Employer employer = new Employer();
		while (result.next()) {
			String name = result.getString(EmployerTable.NAME);
			String address = result.getString(EmployerTable.ADDRESS);
			String website = result.getString(EmployerTable.WEBSITE);
			if (notNullNotEmpty(name)) {
				employer.setName(name);
			}
			if (notNullNotEmpty(address)) {
				employer.setAddress(address);
			}
			if (notNullNotEmpty(website)) {
				employer.setWebsite(website);
			}
			employer.addPhoneNumber(new PhoneNumber(result.getLong(PhoneNumberTable.PHONE_NUMBER)));
		}

		return employer;
	}

	private static StringBuilder joinPhoneNumber() {
		StringBuilder sb = new StringBuilder(" LEFT JOIN ").append(PhoneNumberTable.TITLE).append(" ON ")
				.append(EmployerTable.TITLE).append(".").append(EmployerTable.ID).append(" = ")
				.append(PhoneNumberTable.TITLE).append(".").append(PhoneNumberTable.EMPLOYER);
		return sb;
	}

	@Override
	public Employer getEmployer(String name) {
		Connection conn = getConnection();

		String query = "SELECT "//
				+ EmployerTable.NAME + ", "//
				+ EmployerTable.ADDRESS + ", "//
				+ EmployerTable.WEBSITE + ", " + PhoneNumberTable.TITLE + "." + PhoneNumberTable.PHONE_NUMBER + " as "
				+ PhoneNumberTable.PHONE_NUMBER//
				+ " FROM " + EmployerTable.TITLE//
				+ " INNER JOIN " + PhoneNumberTable.TITLE + " ON " + EmployerTable.TITLE + "." + EmployerTable.NAME
				+ " = " + PhoneNumberTable.TITLE + "." + PhoneNumberTable.EMPLOYER//
				+ " WHERE " + EmployerTable.NAME + " = ? ;";
		try {
			// System.out.println(query + "\n");
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, name);
			ResultSet result = statement.executeQuery();
			return getEmployer(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Job getJob(String adContent) {

		Connection conn = getConnection();
		String query = "SELECT * FROM " + JobTable.TITLE + " where " + JobTable.CONTENT + " = ?";
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, adContent);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				// logResult(result);
				Job j = parseJob(result);
				return j;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int getJobId() {
		return -1;
	}

	@Override
	public List<Job> getJobs(Employer employer) {

		List<Job> list = new ArrayList<Job>();
		Connection conn = getConnection();
		String query = "SELECT * FROM " + JobTable.TITLE + " where " + JobTable.EMPLOYER + " = ?";
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, employer.getName());

			ResultSet result = statement.executeQuery();
			while (result.next()) {
				Job j = parseJob(result);
				list.add(j);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public void load() {
		getConnection();
	}

	private Job parseJob(ResultSet result) throws SQLException {

		int id = result.getInt(JobTable.ID);
		int employerId = result.getInt(JobTable.EMPLOYER);
		Employer employer = getEmployer(employerId);
		String content = result.getString(JobTable.CONTENT);
		String datePosted = result.getString(JobTable.DATE_POSTED);
		String lastSeen = result.getString(JobTable.LAST_SEEN);

		Job j = new Job(id, employer, content, LocalDate.parse(datePosted), LocalDate.parse(lastSeen));

		return j;
	}

	@Override
	public void remove(Employer employer) {
		Connection conn = getConnection();

		String query = "DELETE FROM " + EmployerTable.TITLE + " WHERE " + EmployerTable.NAME + " = ?";
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, employer.getName());
			int eu = statement.executeUpdate();
			System.out.println("Delete result: " + eu);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void remove(Job job) {
		Connection conn = getConnection();
		String query = "DELETE FROM " + JobTable.TITLE + " WHERE " + JobTable.ID + " = ?";
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, job.getId());

			int eu = statement.executeUpdate();
			System.out.println("Delete result: " + eu);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void save() {
		// This method intentionally blank
	}

	public SqliteDB(File file) {
		super();
		this.file = file;
	}

	public SqliteDB() {
		this(DEFAULT);
	}
}
