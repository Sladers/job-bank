package local.slade.db;

import java.util.List;

import local.slade.structs.Employer;
import local.slade.structs.Job;

public class CollectionDBSkeleton {
	public List<Employer> employers;
	public List<Job> jobs;

	public List<Employer> getEmployers() {
		return employers;
	}

	public void setEmployers(List<Employer> employers) {
		this.employers = employers;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public CollectionDBSkeleton() {
		// blank
	}

	public CollectionDBSkeleton(List<Employer> employers, List<Job> jobs) {
		this.employers = employers;
		this.jobs = jobs;
	}
}
