package local.slade.db;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import local.slade.scraping.ChamberOfCommerce;
import local.slade.structs.Employer;
import local.slade.structs.Job;

public class CollectionDB extends DbConnection {

	private transient ObservableList<Job> jobs = FXCollections.observableList(new ArrayList<Job>());

	private transient ObservableList<Employer> employers = FXCollections.observableList(new ArrayList<Employer>());

	private transient ListProperty<Job> jobsProperty = new SimpleListProperty<Job>(jobs);

	private transient int idCounter = 0;

	@Override
	public void addAll(Collection<Job> e) {
		for (Job j : e) {
			addJob(j);
		}
	}

	@Override
	public void addEmployer(Employer employer) {
		if (!employers.contains(employer)) {
			employers.add(employer);
		}
	}

	@Override
	public void addJob(Job job) {
		if (job != null) {

			// check to see if we have this job already
			// look by id
			Job old = getJob(job.getId());

			// look for the job by ad content if we didn't find it by id
			if (old == null) {
				old = getJob(job.getAdContent());
			}
			if (old != null) {
				// update the job if it exists already
				old.setLastSeen(LocalDate.now());
				old.setImage(job.getImage());
				old.setLink(job.getLink());
			} else {
				if (job.getId() == -1) {
					job.setId(getJobId());
				}
				jobs.add(job);
			}
		}
	}

	@Override
	public int countEmployers() {
		return employers.size();
	}

	@Override
	public int countJobs() {
		return jobs.size();
	}

	@Override
	public ObservableList<Employer> getAllEmployers() {
		return employers;
	}

	@Override
	public List<Job> getAllJobs() {
		return jobs;
	}

	@Override
	public Employer getEmployer(String name) {
		if (name != null) {
			if (name.endsWith("...")) {
				name = name.substring(0, name.length() - 3);
			}
			name = name.toLowerCase();
			for (Employer e : getAllEmployers()) {
				if (e.getName() != null && e.getName().toLowerCase().startsWith(name)) {
					return e;
				}
			}
		}
		return null;
	}

	@Override
	public int getJobId() {
		while (getJob(idCounter) != null) {
			idCounter++;
		}

		return idCounter++;
	}

	public Job getJob(int id) {
		for (Job j : getAllJobs()) {
			if (j.getId() == id) {
				return j;
			}
		}
		return null;
	}

	@Override
	public Job getJob(String adContent) {
		for (Job j : jobs) {
			if (j.getAdContent().equalsIgnoreCase(adContent)) {
				return j;
			}
		}
		return null;
	}

	@Override
	public List<Job> getJobs(Employer employer) {
		List<Job> list = new ArrayList<Job>();
		for (Job j : jobs) {
			if (j.getEmployer().equals(employer)) {
				list.add(j);
			}
		}
		return list;
	}

	public ListProperty<Job> jobsProperty() {
		return jobsProperty;
	}

	public static void main(String[] args) {
	}

	private void loadFromJson(File f) {
		if (f.exists()) {
			System.out.println("Loading from JSON");
			System.out.println("Got file to import from");
			ObjectMapper mapper = getMapper();

			try {
				CollectionDBSkeleton skeleton = mapper.readValue(f, CollectionDBSkeleton.class);
				jobs.setAll(skeleton.jobs);
				employers.clear();
				// for (Employer e : ChamberOfCommerce.loadEmployers()) {
				// addEmployer(e);
				// }
				for (Employer e : skeleton.employers) {
					addEmployer(e);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			employers.clear();
			try {
				for (Employer e : ChamberOfCommerce.loadEmployers()) {
					addEmployer(e);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void load() {
		// find the most recent json save file
		DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE;
		Pattern p = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
		Matcher m;
		LocalDate mostRecent = LocalDate.ofEpochDay(0);
		LocalDate temp;
		for (File f : new File("./Saves/").getAbsoluteFile().listFiles()) {
			if (f.isFile()) {
				String fileName = f.getName();
				if (fileName.endsWith(".json")) {
					m = p.matcher(fileName);
					if (m.find()) {
						temp = LocalDate.parse(m.group());
						if (temp.isAfter(mostRecent)) {
							mostRecent = temp;
						}
					}
				}
			}
		}

		File f = new File("./Saves/JobListDB." + dtf.format(mostRecent) + ".json");

		loadFromJson(f);
		updateJobStatus();
	}

	@Override
	public void remove(Employer employer) {
		employers.remove(employer);

	}

	@Override
	public void remove(Job job) {
		jobs.remove(job);
	}

	private static ObjectMapper MAPPER;

	private static ObjectMapper getMapper() {
		if (MAPPER == null) {
			MAPPER = new ObjectMapper();
			MAPPER.findAndRegisterModules();
			MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		}
		return MAPPER;
	}


	private static void cleanupSaveFiles() {
		File folder = new File("./Saves/");

		Pattern p = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
		Matcher m;

		LocalDate today = LocalDate.now();

		// List<LocalDate> list = new ArrayList<>();
		List<Pair<File, LocalDate>> list = new ArrayList<>();
		for (File f : folder.listFiles()) {
			m = p.matcher(f.getName());
			if (m.find()) {
				LocalDate date = LocalDate.parse(m.group(), DateTimeFormatter.ISO_LOCAL_DATE);
				Pair<File, LocalDate> pair = new Pair<>(f, date);
				list.add(pair);
				System.out.println(pair);
			}
		}

		Pair<File, LocalDate>[] daily = new Pair[7];
		Pair<File, LocalDate>[] weeks = new Pair[4];

		Month thisMonth = today.getMonth();

		int year = today.getYear();

		Month[] months = Month.values();

		Map<String, Pair<File, LocalDate>> map = new HashMap<>();

		for (int i = thisMonth.ordinal() + 1; i < 12; i++) {
			map.put(months[i] + " " + (year - 1), null);
		}

		for (int i = 0; i <= thisMonth.ordinal(); i++) {
			map.put(months[i] + " " + year, null);
		}

		int index;
		for (Pair<File, LocalDate> date : list) {
			int difference = (int) ChronoUnit.DAYS.between(date.getValue(), today);
			if (difference >= 0 && difference < 7) {
				// keep daily for the last week
				daily[difference] = date;
			} else if (difference >= 7 && difference < 31) {
				// weekly for the last month
				index = (difference / 7) - 1;
				weeks[index] = date;
			} else if (difference >= 31 && difference < 365) {
				// monthly for the last year
				String key = date.getValue().getMonth() + " " + date.getValue().getYear();
				if (map.keySet().contains(key)) {
					map.put(key, date);
				}
			}
		}
		Set<Pair<File, LocalDate>> keep = new HashSet<>();

		for (Pair<File, LocalDate> d : daily) {
			if (d != null && d.getValue() != null) {
				System.out.println("Keeping: " + d);
				keep.add(d);
			}
		}

		for (Pair<File, LocalDate> d : weeks) {
			if (d != null && d.getValue() != null) {
				System.out.println("Keeping: " + d);
				keep.add(d);
			}
		}

		for (Pair<File, LocalDate> d : map.values()) {
			if (d != null && d.getValue() != null) {
				System.out.println("Keeping: " + d);
				keep.add(d);
			}
		}

		for (Pair<File, LocalDate> d : list) {
			if (!keep.contains(d)) {
				System.out.println("Deleting: " + d.getKey());
				d.getKey().delete();
			}
		}
	}

	@Override
	public void save() {
		cleanupSaveFiles();
		File saveFile = new File("./Saves/JobListDB." + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".json");
		if (saveFile.exists()) {
			saveFile.delete();
		}
		try {
			saveFile.createNewFile();
			ObjectMapper mapper = getMapper(); 

			mapper.findAndRegisterModules();
			mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
			for (Job j : getAllJobs()) {
				// clear any non UTF-8 characters
				j.setAdContent(j.getAdContent());
			}
			System.out.println("Got a save file");

			try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(saveFile),
					StandardCharsets.UTF_8)) {
				String jsonString = mapper.writerWithDefaultPrettyPrinter()
						.writeValueAsString(new CollectionDBSkeleton(employers, jobs));
				out.write(jsonString);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		oos.writeObject(new ArrayList<Job>(jobs));
		Iterator<Employer> it = employers.iterator();
		while (it.hasNext()) {
			Employer e = it.next();
			if (e == null) {
				it.remove();
			} else if (e.getName() == null && e.getAddress() == null && e.getWebsite() == null
					&& (e.getPhoneNumber() == null || e.getPhoneNumber().size() == 0)) {
				it.remove();
			}
		}
		oos.writeObject(new ArrayList<Employer>(employers));
	}

}
