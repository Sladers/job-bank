package local.slade.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import local.slade.scraping.Email;
import local.slade.structs.Employer;
import local.slade.structs.Job;
import local.slade.structs.PhoneNumber;

public abstract class DbConnection {

	protected transient IntegerProperty activeJobs = new SimpleIntegerProperty();

	protected transient IntegerProperty freshJobs = new SimpleIntegerProperty();

	public IntegerProperty activeJobsProperty() {
		return activeJobs;
	}

	/**
	 * Adds all of the given jobs to the DB.
	 *
	 * @param e
	 */
	public abstract void addAll(Collection<Job> e);

	/**
	 * Adds the given employer to the DB if not already present
	 *
	 * @param employer
	 */
	public abstract void addEmployer(Employer employer);

	/**
	 * Adds the given job to the DB if not already present
	 *
	 * @param job
	 */
	public abstract void addJob(Job job);

	/**
	 * Returns the number of employers in the DB
	 *
	 * @return
	 */
	public abstract int countEmployers();

	/**
	 * Returns the number of jobs in the DB
	 *
	 * @return
	 */
	public abstract int countJobs();

	public IntegerProperty freshJobsProperty() {
		return freshJobs;
	}

	public int getActiveJobs() {
		return activeJobs.get();
	}

	/**
	 * Return a list of all employers in the DB
	 *
	 * @return
	 */
	public abstract ObservableList<Employer> getAllEmployers();

	/**
	 * Returns a list of all jobs in the DB
	 *
	 * @return
	 */
	public abstract List<Job> getAllJobs();

	/**
	 * Return a Employer containing the given phone numbers, address, and
	 * preferred website. Creates a new Employer if one does not exist.
	 *
	 * @param phoneNumbers
	 * @param websites
	 * @param address
	 * @return
	 */
	public Employer getEmployer(List<PhoneNumber> phoneNumbers, List<String> websites, String address) {
		Employer e = null;

		// see if there is an existing employer with one of the given phones
		// numbers or websites
		List<Employer> employers = getAllEmployers();
		outer: for (Employer temp : employers) {
			for (PhoneNumber n : phoneNumbers) {
				if (n != null && temp.getPhoneNumber().contains(n)) {
					e = temp;
					break outer;
				}
			}
			for (String w : websites) {
				if (temp.getWebsite() != null && temp.getWebsite().equals(w)) {
					e = temp;
					break outer;
				}
			}
			if (address != null && address.equalsIgnoreCase(temp.getAddress())) {
				System.out.println("Found employer by existing address: " + address);
				e = temp;
				break outer;
			}
		}

		// pick a website from the options provided, prioritize non-public email
		// addresses
		String winner = null;
		for (String w : websites) {
			if (winner == null) {
				winner = w;
			} else if (Email.isPublic(winner)) {
				winner = w;
			}
		}

		if (e != null) {

			// add any missing phone numbers
			for (PhoneNumber n : phoneNumbers) {
				e.addPhoneNumber(n);
			}

			if ((e.getAddress() == null || e.getAddress().isEmpty()) && address != null) {
				System.out.println("Udating address: " + address);
				e.setAddress(address);
			}

			if ((e.getWebsite() == null || e.getWebsite().isEmpty()) && winner != null) {
				System.out.println("Updating website: " + winner);
				e.setWebsite(winner);
			}
			// add a new employer only if it contains a non-null field
		} else {//if ((phoneNumbers != null && phoneNumbers.size() > 0) || address != null || winner != null) {
			e = new Employer(null, phoneNumbers, address, winner);
			// System.out.println("Created new employer: " + e);
			employers.add(e);
		}

		return e;
	}

	/**
	 * Returns an Employer matching any of the given phoneNumbers, websites, or
	 * address. If no matching employer exists a new one is created with the
	 * given information. If an existing Employer is foudn they are updated to
	 * include all of the given information
	 *
	 * @param phoneNumber
	 * @param website
	 * @param address
	 * @return
	 */
	// public abstract Employer3 getEmployer(List<PhoneNumber> phoneNumber,
	// List<String> website, String address);

	/**
	 * Returns an employer with the given name. If no matching Employer is found
	 * none is returned
	 *
	 * @param name
	 * @return
	 */
	public abstract Employer getEmployer(String name);

	public int getFreshJobs() {
		return freshJobs.get();
	}

	/**
	 * Return a job with the given ad content, if one exists
	 *
	 * @param adContent
	 * @return
	 */
	public abstract Job getJob(String adContent);

	/**
	 * Returns the next available id
	 *
	 * @return
	 */
	public abstract int getJobId();

	/**
	 * Return a list of all jobs with the given employer
	 *
	 * @param employer
	 * @return
	 */
	public abstract List<Job> getJobs(Employer employer);

	/**
	 *
	 * @return a list of all jobs that have not been seen today
	 */
	public List<Job> getStaleJobs() {
		List<Job> stale = new ArrayList<Job>();

		LocalDate today = LocalDate.now();

		for (Job j : getAllJobs()) {
			if (j.getLastSeen().isBefore(today)) {
				stale.add(j);
			}
		}

		return stale;
	}

	/**
	 * Load the
	 */
	public abstract void load();

	/**
	 * Removes the employer from list of Employers and deletes this employer
	 * form any jobs with this employer
	 *
	 * @param employer
	 */
	public abstract void remove(Employer employer);

	/**
	 * Removes the given job
	 *
	 * @param job
	 */
	public abstract void remove(Job job);

	public abstract void save();

	public void setActiveJobs(int value) {
		this.activeJobs.set(value);
	}

	public void setFreshJobs(int value) {
		freshJobs.set(value);
	}

	public void updateJobStatus() {
		activeJobs.set(0);
		freshJobs.set(0);
		LocalDate today = LocalDate.now();

		for (Job j : getAllJobs()) {
			if (j.getLastSeen().isEqual(today)) {
				activeJobs.set(activeJobs.get() + 1);
			}
			if (j.getDatePosted().isEqual(today)) {
				freshJobs.set(freshJobs.get() + 1);
			}
		}
	}
}
