package local.slade.structs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Address {
	public static String PO_BOX = "P([\\.\\s])?O([\\\\.\\\\s])? Box \\d+";
	public static String ROAD = "\\d+ [\\w\\s]+ (Ave(nue)?|St(reet)?|R(oa)?d|Pl(ace)?|L(a)?n(e)?|Dr(ive)?)( East| West| South| North)?";

	public static Pattern regex() {
		return Pattern.compile(PO_BOX + "|" + ROAD, Pattern.CASE_INSENSITIVE);
	}

	public static boolean isAddress(String s) {
		return regex().matcher(s).matches();
	}

	public static String address(String s) {
		Matcher m = regex().matcher(s);
		if (m.find()) {
			String address = m.group();
			System.out.println(address);
			return address;
		}
		return null;
	}

	public static void main(String[] args) {
		String[] ss = { "PO Box 812\nFort Frances, ON\nP9A3V4", //
				"P.O. Box 812\nFort Frances, ON\nP9A3V4", //
				"448 Third St East\nFort Frances, ON\nP9A3V4", //
				"448 Third Street East\nFort Frances, ON\nP9A3V4", //
				"448 Third St. E\nFort Frances, ON\nP9A3V4",//
		};

		for (String s : ss) {
			System.out.println(address(s));
		}
	}
}
