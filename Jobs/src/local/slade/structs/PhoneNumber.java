package local.slade.structs;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PhoneNumber implements Serializable, Comparable<PhoneNumber> {

	private static final long serialVersionUID = 1L;
	private static final String AREA_CODE = "((\\()?[0-9]{3}(\\))?([ -])?)?";
	private static final String MIDDLE = "[0-9]{3}([ -]+)?[0-9]{4}";
	public static final Pattern REGEX = Pattern.compile(AREA_CODE + MIDDLE);

	public static boolean valid(String entry) {
		if (entry != null) {
			entry = entry.replaceAll("\\W", "");
			if (entry.length() >= 7) {
				Pattern p = Pattern.compile(".*\\d.*");
				if (p.matcher(entry).matches()) {
					p = Pattern.compile("[A-Za-z]+");
					if (p.matcher(entry).find()) {
						String[] split = entry.split(p.pattern());
						return split[0].length() == 10 || split[0].length() == 7;
					} else {
						return entry.length() == 10 || entry.length() == 7;
					}
				}
			}
		}

		return false;
	}

	public static void main(String[] args) {
		// System.out.println(new PhoneNumber(8072746671L));
		// System.out.println(new PhoneNumber(8072711572L));
		// System.out.println(new PhoneNumber("(807)271-1572"));
		// System.out.println(new PhoneNumber("(801) 274-6671ext105"));
		// System.out.println(new PhoneNumber("(801) 274-6671 ext 105"));
		// System.out.println(new PhoneNumber(new PhoneNumber("(801)
		// 274-6671ext105").toString()));
		// System.out.println(new PhoneNumber(2749971));
		// System.out.println(new PhoneNumber("2749971"));
		Pattern p = Pattern.compile(AREA_CODE + MIDDLE);
		String[] ss = { "807", "00", "(807)", "(80", "(807", "8072746671", "(807)2726671", "(807) 274-6671",
				"807-274-6671", "or 275-7065." };
		for (String s : ss) {
			Matcher m = p.matcher(s);
			System.out.println(s + ": " + m.find());
		}
	}

	private final int extension;
	private final long number;

	public PhoneNumber(long number) {
		this(number, -1);
	}

	@JsonCreator
	public PhoneNumber(@JsonProperty("number") long number, @JsonProperty("extension") int extension) {
		if (number <= 9999999L) {
			number += 8070000000L;
		}
		this.number = number;
		this.extension = extension;
	}

	public PhoneNumber(String number) {
		if (number != null) {
			number = number.replaceAll("\\W", "");
			String[] split = number.split("[A-Za-z]+");
			if (split.length > 0) {
				long temp = Long.parseLong(split[0]);
				if (temp <= 9999999) {
					temp += 8070000000L;
				}
				this.number = temp;
				if (split.length > 1 && split[1].length() > 0) {
					extension = Integer.parseInt(split[1]);
				} else {
					extension = -1;
				}
			} else {
				throw new IllegalArgumentException("Bad number format." + number);
			}
		} else {
			throw new IllegalArgumentException("Null entry");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + extension;
		result = prime * result + (int) (number ^ (number >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof PhoneNumber && equals((PhoneNumber) other);
	}

	public boolean equals(PhoneNumber other) {
		return other != null && number == other.number && extension == other.extension;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		String number = Long.toString(this.number);
		sb.append("(");
		sb.append(number.subSequence(0, 3));
		sb.append(") ");
		sb.append(number.subSequence(3, 6));
		sb.append("-");
		sb.append(number.subSequence(6, 10));
		if (extension != -1) {
			sb.append(" ext ");
			sb.append(extension);
		}
		return sb.toString();
	}

	public long getNumber() {
		return number;
	}

	public int getExtension() {
		return extension;
	}

	@Override
	public int compareTo(PhoneNumber other) {
		if (other != null) {
			return Long.compare(number, other.number);
		} else {
			return 1;
		}

	}
}
