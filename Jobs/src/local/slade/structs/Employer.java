package local.slade.structs;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javafx.beans.property.*;
import javafx.collections.FXCollections;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Employer implements Serializable, Comparable<Employer> {

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		out.writeUTF(name.getValueSafe());
		if (phoneNumber == null) {
			System.out.println("WTF phonenumber is null");
		}
		if (phoneNumber.get() == null) {
			System.out.println("WTF phonenumber.get() is null");
		}
		out.writeObject(new ArrayList<PhoneNumber>(phoneNumber.get()));
		out.writeUTF(address.getValueSafe());
		out.writeUTF(website.getValueSafe());
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		name = new SimpleStringProperty(in.readUTF());
		List<PhoneNumber> pn = (ArrayList<PhoneNumber>) in.readObject();

		phoneNumber = new SimpleListProperty<PhoneNumber>(FXCollections.observableList(pn));
		address = new SimpleStringProperty(in.readUTF());
		website = new SimpleStringProperty(in.readUTF());
	}

	private static final long serialVersionUID = 1L;

	private static <T> boolean containAnEqualElement(Collection<T> a, Collection<T> b) {

		Iterator<T> it = a.iterator();
		while (it.hasNext()) {
			if (b.contains(it.next())) {
				return true;
			}
		}

		return false;
	}

	private transient StringProperty name = new SimpleStringProperty();

	private transient ListProperty<PhoneNumber> phoneNumber = new SimpleListProperty<PhoneNumber>();

	private transient StringProperty address = new SimpleStringProperty();

	private transient StringProperty website = new SimpleStringProperty();

	/**
	 * not used
	 */
	public Employer() {
		this(null, null, null, null);
	}

	public Employer(String name) {
		this(name, null, null, null);
	}

	public Employer(String name, List<PhoneNumber> phoneNumber, String address, String website) {
		setName(name);
		setPhoneNumber(phoneNumber);
		setAddress(address);
		setWebsite(website);
	}

	public void addPhoneNumber(PhoneNumber n) {
		if (!phoneNumber.contains(n)) {
			phoneNumber.add(n);
		}
	}

	private static <T extends Comparable<T>> int compare(Property<T> a, Property<T> b) {
		T t = null;
		try {
			if (a != null && a.getValue() != null) {
				t = a.getValue();
				if (b != null) {
					return compare(t, b.getValue());
				} else {
					return 1;
				}
			} else if (b != null && b.getValue() != null) {
				return -1;
			} else {
				return 0;
			}
		} catch (NullPointerException e) {
			System.err.println("Caught Nullpointer in Employer3.compare(Property a, Property b)");
			System.err.println("T: " + t);
			if (t != null) {
				System.err.println("Class of T: " + t.getClass());
			}
			System.err.println("a: " + a);
			if (a != null) {
				System.err.println("a.getValue(): " + a.getValue());
			}

			System.err.println("b: " + b);
			if (b != null) {
				System.err.println("b.getValue(): " + b.getValue());
			}
			throw e;
		}
	}

	private static <T> int compare(Comparable<T> a, T b) {
		if (a != null) {
			if (b == null) {
				return 1;
			} else {
				return a.compareTo(b);
			}
		} else if (b != null) {
			return -1;
		} else {
			return 0;
		}
	}

	@Override
	public int compareTo(Employer other) {

		if (other == null) {
			return 1;
		}

		int val = compare(name, other.name);
		if (val == 0) {
			val = compare(website, other.website);
			if (val == 0) {
				val = compare(address, other.address);
				if (val == 0) {
					val = compare(getFirstNumber(), other.getFirstNumber());
				}
			}
		}
		return val;
	}

	@Deprecated
	public int compareToOld(Employer other) {
		try {
			if (other == null) {
				// System.out.println("other is null");
				return 1;
			}

			if (name != null && name.getValue() != null) {
				// System.out.println("Name is good");
				if (other.name != null && other.name.getValue() != null) {
					// System.out.println("Comparing names");
					name.getValue().compareTo(other.name.getValue());
				} else {
					// System.out.println("other has a bad name");
					return -1;
				}
			} else if (other.name != null && other.name.get() != null) {
				// System.out.println("name is null but other has good name.");
				return 1;
			} else if (website != null && website.get() != null) {
				// System.out.println("Have a good website");
				if (other.website == null || other.website.get() == null) {
					// System.out.println("Other has a bad website");
					return -1;
				} else {
					// System.out.println("Comparing websites");
					return website.get().compareTo(other.website.get());
				}
			} else if (other.website != null && other.website.get() != null) {
				// System.out.println("Other has a good website");
				return 1;
			} else if (phoneNumber.size() > 0) {
				// System.out.println("We have a phone number");
				if (other.phoneNumber.isEmpty()) {
					// System.out.println("Other doesn't have a number");
					return -1;
				} else {
					// System.out.println("Comparing first numbers");
					return firstNumber().toString().compareTo(other.firstNumber().toString());
				}
			} else if (other.phoneNumber.size() > 0) {
				// System.out.println("Other has a phone number but we don't");
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public boolean isEmpty() {

		if (getName() != null && !getName().isEmpty()) {
			return false;
		}
		if (getAddress() != null && !getAddress().isEmpty()) {
			return false;
		}
		if (getWebsite() != null && !getWebsite().isEmpty()) {
			return false;
		}
		if (getPhoneNumber() != null && !getPhoneNumber().isEmpty()) {
			return false;
		}
		return true;
	}

	public boolean equals(Employer other) {
		if (other != null) {
			if (this == other) {
				return true;
			}
			if (isEmpty() && other.isEmpty()) {
				return false;
			}

			if (getName() != null) {
				return getName().equalsIgnoreCase(other.getName());
			}
			if (getAddress() != null) {
				return getAddress().equalsIgnoreCase(other.getAddress());
			}
			if (getWebsite() != null) {
				return getWebsite().equalsIgnoreCase(other.getWebsite());
			}
			if (containAnEqualElement(phoneNumber, other.phoneNumber)) {
				return true;
			}

			return getName() == null && other.getName() == null && getAddress() == null && other.getAddress() == null
					&& getWebsite() == null && other.getWebsite() == null
					&& getPhoneNumber().equals(other.getPhoneNumber());
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Employer && equals((Employer) obj);
	}

	private PhoneNumber firstNumber() {
		PhoneNumber winner = null;
		for (PhoneNumber p : phoneNumber) {
			if (winner == null || p.toString().compareTo(winner.toString()) > 0) {
				winner = p;
			}
		}

		return winner;
	}

	public String getAddress() {
		return address.get();
	}

	public String getName() {
		return name.get();
	}

	public List<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * 
	 * @return the first PhoneNumber in the list
	 */
	public PhoneNumber getFirstNumber() {
		if (phoneNumber != null && phoneNumber.size() > 0) {
			return phoneNumber.get(0);
		}
		return null;
	}

	public String getWebsite() {
		return website.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public StringProperty addressProperty() {
		return address;
	}

	public StringProperty websiteProperty() {
		return website;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}

	public void setAddress(String address) {
		this.address.set(address);
	}

	public void setName(String name) {
		if (name != null) {
			name = name.trim();
		}
		this.name.set(name);
	}

	public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
		if (phoneNumber != null) {
			this.phoneNumber.set(FXCollections.observableArrayList(phoneNumber));
		} else {
			this.phoneNumber.set(FXCollections.observableArrayList());
		}
	}

	public void setWebsite(String website) {
		if (website != null) {
			website = website.replaceAll("www\\.|\\w+://", "").replaceAll("/([\\w/]+)?$", "");
			if (website.contains("acebook")) {
				website = null;
			}
		}
		this.website.set(website);
	}

	@Override
	public String toString() {

		if (getName() != null && !"".equalsIgnoreCase(getName())) {
			return getName();
		}

		if (getWebsite() != null && !"".equalsIgnoreCase(getWebsite())) {
			return getWebsite();
		}

		if (address != null && address.get() != null) {
			return address.get();
		}

		if (phoneNumber.size() > 0) {
			return phoneNumber.iterator().next().toString();
		}

		return fields();
	}

	public String fields() {
		return "Employer3[name: " + name.get() + ", phoneNumber: " + phoneNumber.get() + ", address: " + address.get()
				+ ", website: " + website.get() + "]";
	}

}
