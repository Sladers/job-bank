package local.slade.structs;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.*;

public class Job implements Serializable, Comparable<Job> {

	public static final Job NULL = new Job(null);

	private static final long serialVersionUID = 1L;

	private transient IntegerProperty id;
	private transient ObjectProperty<Employer> employer;
	private transient StringProperty adContent;
	private transient ObjectProperty<LocalDate> datePosted;
	private transient ObjectProperty<LocalDate> lastSeen;
	private transient StringProperty link;
	private transient StringProperty image;
	private transient BooleanProperty hidden;

	public boolean isHidden() {
		return hidden.get();
	}

	public void setHidden(boolean hidden) {
		this.hidden.set(hidden);
	}

	public BooleanProperty hiddenProperty() {
		return this.hidden;
	}

	public String getLink() {
		return link.get();
	}

	public void setLink(String link) {
		this.link.set(link);
	}

	public StringProperty linkProperty() {

		return link;
	}

	public String getImage() {
		return image.get();
	}

	public void setImage(String image) {
		this.image.set(image);
	}

	public StringProperty imageProperty() {
		return image;
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		out.writeInt(id.get());
		out.writeObject(employer.get());
		out.writeUTF(adContent.getValueSafe());
		out.writeObject(datePosted.get());
		out.writeObject(lastSeen.get());
		out.writeObject(link.get());
		out.writeObject(image.get());
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		int id = in.readInt();
		Employer employer = (Employer) in.readObject();
		String adContent = in.readUTF();
		LocalDate datePosted = (LocalDate) in.readObject();
		LocalDate lastSeen = (LocalDate) in.readObject();

		this.id = new SimpleIntegerProperty(id);
		this.employer = new SimpleObjectProperty<Employer>(employer);
		this.adContent = new SimpleStringProperty(adContent);
		this.datePosted = new SimpleObjectProperty<LocalDate>(datePosted);
		this.lastSeen = new SimpleObjectProperty<LocalDate>(lastSeen);

		try {
			String link = in.readUTF();
			this.link = new SimpleStringProperty(link);
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
			this.link = new SimpleStringProperty();
		}

		try {
			String image = in.readUTF();
			this.image = new SimpleStringProperty(image);
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
			this.image = new SimpleStringProperty();
		}
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public ObjectProperty<Employer> employerProperty() {
		return employer;
	}

	public StringProperty adContentProperty() {
		return adContent;
	}

	public ObjectProperty<LocalDate> datePostedProperty() {
		return datePosted;
	}

	public ObjectProperty<LocalDate> lastSeenProperty() {
		return lastSeen;
	}

	public int getId() {
		return id.get();
	}

	public void setId(int id) {
		this.id.set(id);
	}

	public Employer getEmployer() {
		return employer.get();
	}

	public void setEmployer(Employer employer) {
		this.employer.set(employer);
	}

	public String getAdContent() {
		return adContent.get();
	}

	public void setAdContent(String adContent) {
		if (adContent != null) {
			Charset charset = Charset.forName("UTF-8");
			adContent = charset.decode(charset.encode(adContent)).toString();
		}
		this.adContent.set(adContent);
	}

	public LocalDate getDatePosted() {
		return datePosted.get();
	}

	public void setDatePosted(LocalDate datePosted) {
		this.datePosted.set(datePosted);
	}

	public LocalDate getLastSeen() {
		return lastSeen.get();
	}

	public void setLastSeen(LocalDate lastSeen) {
		this.lastSeen.set(lastSeen);
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Job && equals((Job) obj);
	}

	public boolean equals(Job other) {
		return other != null && id.get() == other.id.get();
	}

	@Override
	public int hashCode() {
		return id.get();
	}

	public Job(String adContent) {
		this(-1, null, adContent);
	}

	public Job(int id, Employer employer, String adContent) {
		this(id, employer, adContent, LocalDate.now(), LocalDate.now());
	}

	@JsonCreator
	public Job(@JsonProperty("id") int id, @JsonProperty("employer") Employer employer,
			@JsonProperty("adContent") String adContent, @JsonProperty("datePosted") LocalDate datePosted,
			@JsonProperty("lastSeen") LocalDate lastSeen, @JsonProperty("link") String link,
			@JsonProperty("image") String image, @JsonProperty("hidden") boolean hidden) {
		this.id = new SimpleIntegerProperty(id);
		this.employer = new SimpleObjectProperty<Employer>(employer);
		this.adContent = new SimpleStringProperty(adContent);
		this.datePosted = new SimpleObjectProperty<LocalDate>(datePosted);
		this.lastSeen = new SimpleObjectProperty<LocalDate>(lastSeen);
		this.link = new SimpleStringProperty(link);
		this.image = new SimpleStringProperty(image);
		this.hidden = new SimpleBooleanProperty(hidden);
	}

	public Job(int id, Employer employer, String adContent, LocalDate datePosted, LocalDate lastSeen) {
		this(id, employer, adContent, datePosted, lastSeen, null, null, false);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Job[id: ");
		sb.append(getId());
		sb.append(", Employer3: ");
		sb.append(getEmployer());
		sb.append(", Posted: " + getDatePosted());
		sb.append(", Last Seen: ");
		sb.append(getLastSeen());
		sb.append("]");
		return sb.toString();
	}

	public static Comparator<Job> idComparator() {
		return new Comparator<Job>() {
			@Override
			public int compare(Job a, Job b) {
				return b.id.get() - a.id.get();
			}
		};
	}

	public static Comparator<Job> employerComparator() {
		return new Comparator<Job>() {

			@Override
			public int compare(Job a, Job b) {
				return a.employer.get().compareTo(b.employer.get());
			}
		};
	}

	@Override
	public int compareTo(Job other) {

		int val = 0;
		val = -1 * getLastSeen().compareTo(other.getLastSeen());
		if (val == 0) {
			val = -1 * getDatePosted().compareTo(other.getDatePosted());
			if (val == 0) {
				Employer e = getEmployer();
				if (e != null) {
					val = getEmployer().compareTo(other.getEmployer());
				} else if (other.getEmployer() != null) {
					return -1;
				}
				if (val == 0) {
					val = Integer.compare(getId(), other.getId());
				}
			}
		}

		return val;
	}
}
