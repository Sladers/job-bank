package local.slade.scraping;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Patterns and methods to help identify and categorize email addresses and
 * domains.
 *
 * @author Jeremy Gruttner<br>
 */
public class Email {

	public static final String website1 = "(?:(?:http://(?:www\\.)?)|(?:www\\.)|(?:@))([\\w-_\\.]+\\.[\\w]+)";
	public static final Pattern website = Pattern.compile(website1//
			+ "|\\w\\w+\\.\\w\\w+\\.\\w\\w+", Pattern.CASE_INSENSITIVE);

	/**
	 * http://emailregex.com/
	 */
	public static final Pattern EMAIL = Pattern.compile(
			"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
			Pattern.CASE_INSENSITIVE);

	public static final Set<String> PUBLIC_EMAIL = new HashSet<String>(
			Arrays.asList("gmail.com", "bellnet.ca", "live.ca", "live.com", "yahoo.ca", "yahoo.com", "hotmail.com",
					"hotmail.ca", "shaw.ca", "tbaytel.net", "jam21.net", "vianet.ca", "outlook.com", "outlook.ca",
					"bell.net", "xplorenet.ca", "xplorenet.com", "kmts.ca"));

	public static boolean isPublic(String domain) {
		if (domain != null) {
			domain = domain.toLowerCase();
			for (String s : PUBLIC_EMAIL) {
				if (s != null && domain.contains(s)) {
					return true;
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		String[] ss = { "@rrfns.com", "www.rrdsb.com", "@atikokanfht.com", "http://newgold.com",
				"www.riversidehealthcare.ca", "@rrfns.com", "www.northwestcommunitylegalclinic.ca", "www.rrdsb.com",
				"@gmail.com", "@hotmail.com", "@coca-cola.com", "@gmail.com", "@rrfns.com", "@live.ca",
				"@copperriverinn.com", "@titanmanufacturing.ca", "@bellnet.ca", "http://newgold.com", "@domtar.com",
				"@outlook.com", "@fortfrances.ca", "http://fortfrances.ca", "@gmail.com", "www.riversidehealthcare.ca",
				"@rrfns.com", "www.ontario.ca", "www.gojobs.gov.on.ca/ContactUs.aspx", "www.ontario.ca",
				"www.gojobs.gov.on.ca/ContactUs.aspx", "www.rrdsb.com", "www.tncdsb.on.ca", "www.cmhaff.ca",
				"www.ontario.ca", "www.gojobs.gov.on.ca/ContactUs.aspx", "www.ontario.ca",
				"www.gojobs.gov.on.ca/ContactUs.aspx", "@fortfrances.ca", "www.rrdsb.com", "@hatch.com", "@outlook.com",
				"http://www.metisnation.org", "i.e", "should.fail", "newgold.com", "@SHAW.CA", "john@aghospital.on.ca",
				"aghospital.on.ca/careers/" };

		Pattern p = website;
		for (String s : ss) {
			Matcher m = p.matcher(s);
			if (m.find()) {
				for (int i = 1; i <= m.groupCount(); i++) {
					System.out.print("\"" + m.group(i) + "\", ");
				}
				System.out.println();
			} else {
				System.out.println("Fail: " + s);
			}
		}
	}

}
