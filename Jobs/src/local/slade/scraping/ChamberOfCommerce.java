package local.slade.scraping;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import local.slade.structs.Employer;
import local.slade.structs.PhoneNumber;

/**
 * 
 * Parses the a table of local businesses on the Fort Frances Chamber of
 * Commerce website and saves it as json.
 */
public class ChamberOfCommerce {

	public static void main(String[] args) {
		try {
			URL url = new URL("http://fortfranceschamber.com/directory");
			Document doc = Jsoup.parse(url, 10000);
			Elements rows = doc.select("tbody>tr");
			System.out.println("Rows: " + rows.size());
			List<Employer> list = new ArrayList<>();
			for (Element row : rows) {
				try {
					Element eTitle = row.selectFirst(".views-field-title>h3");
					Element eLink = row.selectFirst(".views-field-title>a");
					Element eAddress = row.selectFirst(".views-field-field-address");
					Element ePhoneNumbers = row.selectFirst(".views-field-field-phone");
					Employer e = new Employer();
					e.setName(eTitle.text());
					if (eLink != null) {
						e.setWebsite(eLink.attr("href"));
					}
					e.setAddress(eAddress.text());
					String phoneNumbersText = ePhoneNumbers.text();
					Matcher m = PhoneNumber.REGEX.matcher(phoneNumbersText);
					while (m.find()) {
						e.addPhoneNumber(new PhoneNumber(m.group()));
					}
					System.out.println(e.fields());
					list.add(e);
				} catch (NullPointerException e) {
					e.printStackTrace();
					System.err.println(row);
				}
			}

			saveEmployers(list);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static final File file = new File("ChamberBusinesses.json");

	public static List<Employer> loadEmployers() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<Employer> list = mapper.readValue(file, new TypeReference<ArrayList<Employer>>() {
		});

		return list;
	}

	private static void saveEmployers(List<Employer> list) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();

		try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
			String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(list);
			out.write(jsonString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
