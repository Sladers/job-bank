package local.slade.scraping;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.time.LocalDate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import local.slade.db.CollectionDB;
import local.slade.db.DbConnection;
import local.slade.structs.Job;

public class FFTimesScraper extends Scraper {

	public FFTimesScraper(DbConnection db) {
		super(db);
	}

	@Override
	public String title() {
		return "Fort Frances Times";
	}

	// public static void main(String[] args) {
	// resetDb();
	// CollectionDB db = new CollectionDB();
	// db.load();
	// FFTimesScraper scraper = new FFTimesScraper(db);
	// scraper.parseLive();
	// db.save();
	//
	// for (Job j : db.getAllJobs()) {
	// System.out.println(j);
	// }
	//
	// System.out.println("Employers: " + db.countEmployers());
	// System.out.println("Jobs: " + db.countJobs());
	// System.out.println("Stale jobs: " + db.getStaleJobs().size());
	// }

	public static CollectionDB resetDb() {
		new File("HashSet.db").delete();
		CollectionDB db = new CollectionDB();
		FFTimesScraper scraper = new FFTimesScraper(db);
		scraper.parseCached();

		for (Job j : db.getAllJobs()) {
			j.setDatePosted(LocalDate.of(2018, 4, 25));
			j.setLastSeen(LocalDate.of(2018, 5, 1));
		}
		db.save();

		return db;
	}

	@Deprecated
	public void parseCached() {
		File f = new File("fftimesJobs.html");
		try {
			Document doc = Jsoup.parse(f, "UTF-8", "http://www.fftimes.com/");

			parse(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static final String URL = "http://www.fftimes.com/jobs";

	@Deprecated
	public void parse(Document doc) {
		// Elements elements = doc.select(".field-item.even");
		Elements elements = doc.select("a[href*=/jobs/] > img");
		for (Element e : elements) {
			int id = Integer.parseInt(e.parent().attr("href").replaceAll(".*jobs/", ""));
			String s = e.attr("alt");
			char c = 12;
			s = s.replaceAll(Character.toString(c), "").trim();

			Job j = parseAd(s);
			j.setId(id);
			db.addJob(j);
			System.out.println("newline");
		}

	}

	@Override
	protected Job parseAd(Element e) {
		int id;
		try {
			// usually they use a number for the ID but sometimes they use
			// gibberish
			id = Integer.parseInt(e.parent().attr("href").replaceAll(".*jobs/", ""));
		} catch (NumberFormatException ex) {
			id = db.getJobId();
		}
		String s = e.attr("alt");
		char c = 12;
		s = s.replaceAll(Character.toString(c), "").trim();
		if (!s.isEmpty()) {
			Job j = parseAd(s);
			Element imageElement = e.selectFirst("img");
			String imageUrl = imageElement.attr("src");
			String imageName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1).replaceAll("%", "");
			if (!new File("images" + File.separator + imageName).exists()) {
				downloadImageNIO(imageUrl, imageName);
			}
			j.setImage(imageName);
			j.setId(id);
			return j;
		} else
			return null;
	}

	private static void downloadImageNIO(String imageUrl, String imageName) {
		try {
			URL website = new URL(imageUrl);
			try (ReadableByteChannel rbc = Channels.newChannel(website.openStream());
					FileOutputStream fos = new FileOutputStream("images" + File.separator + imageName)) {
				long transferred = fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				if (transferred <= 0) {
					System.out.println("Failed to download image from url: " + website);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Elements getElements(Document doc) {
		return doc.select("a[href*=/jobs/] > img");
	}

	@Override
	protected Document loadPage() throws IOException {
		URL url = new URL(URL);
		URLConnection urlc = url.openConnection();
		InputStream stream = urlc.getInputStream();

		Document doc = Jsoup.parse(stream, "UTF-8", URL);
		return doc;
	}

	public void parseLive() {
		try {
			URL url = new URL(URL);
			URLConnection urlc = url.openConnection();
			InputStream stream = urlc.getInputStream();

			Document doc = Jsoup.parse(stream, "UTF-8", URL);
			parse(doc);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
