package local.slade.scraping;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import local.slade.db.CollectionDB;
import local.slade.db.DbConnection;
import local.slade.structs.Employer;
import local.slade.structs.Job;

public class IndeedScraper extends Scraper {

	private static String BASE = "https://ca.indeed.com";
	private static String URL = BASE + "/jobs?l=Fort+Frances,+ON&radius=100&limit=50";

	public IndeedScraper(DbConnection db) {
		super(db);
	}

	@Override
	protected Document loadPage() throws IOException {
		URL url = new URL(URL);
		URLConnection urlc = url.openConnection();
		InputStream stream = urlc.getInputStream();

		Document doc = Jsoup.parse(stream, "UTF-8", URL);
		return doc;
	}

	@Override
	public String title() {
		return "Indeed.com";
	}

	@Override
	protected Elements getElements(Document doc) {
		return doc.select(".result");
	}

	@Override
	protected Job parseAd(Element element) throws IOException {

		Element company = element.selectFirst(".company");
		Element summary;// = element.selectFirst(".summary");
		Element link = element.selectFirst("a[href]");
		Element location = element.selectFirst(".location");
		Element title = element.selectFirst(".jobtitle");

		Element date = element.selectFirst(".date");
		int days = 0;
		if (date != null) {
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(date.text());
			if (m.find()) {
				days = Integer.parseInt(m.group());
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<b>");
		sb.append(title.text());
		sb.append("</b><br>");
		sb.append(company.text());
		sb.append("<br>");
		sb.append(location.text());
		sb.append("<br>");

		try {
			if (location.text().contains(", ON")) {
				Document adDoc = Jsoup.parse(new URL(BASE + link.attr("href")), 10000);
				summary = adDoc.selectFirst(".summary");
				if (summary == null) {
					summary = adDoc.selectFirst(".jobsearch-JobComponent-description");
				}
				if (summary != null) {
					// System.out.println(summary.text());
					Job j = parseAd(sb.toString());
					Employer employer = db.getEmployer(company.text());
					sb.append(summary);
					if (employer == null) {
						System.out.println("Failed to find an existing employer name: \"" + company.text() + "\"");
						Employer temp = j.getEmployer();
						if (temp != null) {
							if (temp.getName() == null) {
								System.out.println("Updating name of Employer found by phone or website");
								temp.setName(company.text());
							}
							employer = temp;
						} else {
							System.out
									.println("Creating a new Employer using on their name: \"" + company.text() + "\"");
							employer = new Employer(company.text());
						}
					} else {
						// use an existing Employer that already has the name
						// set
					}
					db.addEmployer(employer);
					j.setEmployer(employer);
					Thread.sleep(100);
					j.setId(db.getJobId());
					j.setDatePosted(LocalDate.now().minusDays(days));
					return j;
				} else {
					System.out.println("Failed to find job summary element ");
					System.out.println(adDoc);
				}
			} else {
				System.out.println("Job posting doesn't appear to be local: " + location.text());
				return Job.NULL;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) throws IOException {
		IndeedScraper scraper = new IndeedScraper(new CollectionDB());
		Document doc = scraper.loadPage();
		Elements ads = scraper.getElements(doc);

		for (Element e : ads) {
			// scraper.parseAd(e);
			System.out.println(e);
			System.out.println();
		}
		// List<Job> list = scraper.normalCall(false);
		// for (Job j : list) {
		// System.out.println(j);
		// }
	}
}
