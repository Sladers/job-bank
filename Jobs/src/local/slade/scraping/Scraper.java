package local.slade.scraping;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;

import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javafx.concurrent.Task;
import local.slade.db.DbConnection;
import local.slade.structs.Address;
import local.slade.structs.Employer;
import local.slade.structs.Job;
import local.slade.structs.PhoneNumber;

/**
 * To provide methods for parsing the ads for phone numbers, websites, and
 * employers so that subclasses can focus on pulling the actual ad/job posting
 * from the website
 * 
 * @author Slade
 *
 */
public abstract class Scraper extends Task<List<Job>> {

	public Scraper(DbConnection db) {
		this.db = db;
	}

	protected final DbConnection db;

	private static String nonNullGroup(Matcher m) {
		for (int i = 1; i <= m.groupCount(); i++) {
			if (m.group(i) != null) {
				return m.group(i);
			}
		}
		return m.group();
	}

	abstract protected Document loadPage() throws IOException;

	public Job parseAd(String s) {

		Job job = new Job(s);
		List<PhoneNumber> numbers = new ArrayList<PhoneNumber>();
		List<String> websites = new ArrayList<String>();
		String address = null;

		try (Scanner scan = new Scanner(s)) {
			String line = null;
			Matcher m;
			String root;
			String tempAddress = null;

			// check each line of the ad for a phone number, website, or address
			while (scan.hasNextLine()) {
				line = scan.nextLine();

				// check for a phone number
				m = PhoneNumber.REGEX.matcher(line);
				if (m.find()) {
					numbers.add(new PhoneNumber(m.group()));
				}

				// check for a website
				m = Email.website.matcher(line);
				if (m.find()) {
					root = nonNullGroup(m);

					// check if the domain is a public or private domain
					if (Email.PUBLIC_EMAIL.contains(root)) {

						// include the entire email if it's public
						m = Email.EMAIL.matcher(line);
						if (m.find()) {
							websites.add(m.group());
						}
					} else {
						// private domain so just add the domain
						websites.add(root);
					}
				}
				tempAddress = Address.address(line);
				if (tempAddress != null) {
					address = tempAddress;
				}
			}
		} finally {
		}

		Employer employer = db.getEmployer(numbers, websites, address);

		job.setEmployer(employer);

		return job;
	}

	protected abstract Elements getElements(Document doc);

	protected abstract Job parseAd(Element e) throws IOException;

	@Override
	protected List<Job> call() {
		try {
			return callQueue();
		} catch (Exception e) {
			updateMessage(e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}

	public abstract String title();

	protected void log(Object o) {
		System.out.println("[" + title() + "] " + o);
	}

	@Override
	protected void updateMessage(String message) {
		log(message);
		super.updateMessage(message);
	}

	@Override
	protected void updateTitle(String title) {
		log(title);
		super.updateTitle(title);
	}

	protected List<Job> callQueue() {

		List<Job> list = new ArrayList<Job>();

		Deque<Element> deck = new ArrayDeque<>();
		updateTitle(title());

		updateMessage("Loading page");
		updateProgress(0, 100);
		try {
			Document doc = loadPage();

			deck.addAll(getElements(doc));

			updateProgress(50, 100);
			int loops = 0;
			int size = deck.size();
			int pro = 0;
			while (deck.size() > 0 && !isCancelled()) {
				loops++;
				if (loops % size == 0) {
					log("Sleeping in deck loop");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				Element e = deck.removeFirst();
				try {
					pro = size - deck.size();
					updateProgress(50 + pro * 50 / size, 100);
					updateMessage("Parsing " + pro + "/" + size);
					Job j = parseAd(e);
					if (j != null && j != Job.NULL) {
						list.add(j);
					} else if (j == null) {
						log("Failed to parse job from element: " + e);
					}
				} catch (IOException ex) {
					if (ex instanceof HttpStatusException) {
						deck.addLast(e);
					}
				}
			}
			if (!isCancelled()) {
				System.out.println(title() + " Scraper cancelled");
			}
			updateProgress(100, 100);
			updateMessage("Parsed " + size + " posts, found " + list.size() + " jobs");
		} catch (IOException e) {
			updateMessage(e.toString());
		}
		return list;
	}

}
