package local.slade.scraping;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import local.slade.db.CollectionDB;
import local.slade.db.DbConnection;
import local.slade.structs.Employer;
import local.slade.structs.Job;
import local.slade.structs.PhoneNumber;

public class ConfederationCollegeScraper extends Scraper {

	public ConfederationCollegeScraper(DbConnection db) {
		super(db);
	}

	private static final String base = "https://confederationc.hua.hrsmart.com";

	private WebDriver driver = null;

	private WebDriver getDriver() {
		if (driver == null) {
			ChromeOptions options = new ChromeOptions();
			System.setProperty("webdriver.chrome.driver",
					"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
			options.addArguments("--headless", "--disable-gpu");
			driver = new ChromeDriver(options);

		}
		return driver;
	}

	private Document selenium(URL url) {
		WebDriver driver = getDriver();

		driver.navigate().to(url);

		return Jsoup.parse(driver.getPageSource());

	}

	@Override
	protected Document loadPage() throws IOException {
		URL url = new URL(base + "/hrsmart/ats/JobSearch/viewAll");
		return Jsoup.parse(url, 10000);

	}

	@Override
	protected List<Job> callQueue() {
		List<Job> list = super.callQueue();
		WebDriver driver = getDriver();
		driver.close();
		return list;
	}

	@Override
	protected Elements getElements(Document doc) {
		return doc.select("tbody tr");
	}

	@Override
	protected Job parseAd(Element e) throws IOException {
		String name = "Confederation College";
		Employer employer = db.getEmployer(name);
		if (employer == null) {
			String address = "440 McIrvine Road";
			List<PhoneNumber> phoneNumber = Arrays.asList(new PhoneNumber("	8072745395"));
			List<String> website = Arrays.asList("confederationcollege.ca");
			employer = db.getEmployer(phoneNumber, website, address);
			employer.setName("Confederation College");
		}
		Element title = e.child(0);
		Element posted = e.child(2);
		// System.out.println("Title:" + title);
		// System.out.println("Posted: " + posted.text());

		// <tr>
		// <td><a href="/hrsmart/ats/Posting/view/914"> <span>Flight Operations
		// Technologist</span> </a></td>
		// <td> CA<br>Thunder Bay Campus Thunder Bay, ON, CA (Primary)<br> </td>
		// <td>7/4/2017</td> /* month/day/year */
		LocalDate postedDate = LocalDate.parse(posted.text(), DateTimeFormatter.ofPattern("M/d/y"));
		// System.out.println(postedDate);
		// <td>School of Aviation</td>
		// <td>SU-F-17-13</td>
		// <td class="actions_td"><a
		// href="/ats/lens.php?action=find_similar_jobs&amp;requisition=914"
		// class="btn btn-default btn-sm" title="Find Matching Jobs"
		// data-toggle="tooltip" data-container="body"><i class="fa
		// fa-binoculars text-black "></i></a></td>
		// </tr>
		String href = title.selectFirst("a").attr("href");
		// System.out.println("href: " + href);
		URL url = new URL(base + href);
		// System.out.println(url);
		Document doc = selenium(url);

		// Elements list = doc.select("fieldset.form");
		// System.out.println("fieldset.form: "+list.size());
		// list = doc.select("fieldset");
		// System.out.println("fieldset: "+list.size());

		Element bigAd = doc.selectFirst("fieldset.form");
		// System.out.println(bigAd);
		// Job job = parseAd(bigAd.toString());
		Job j = new Job(-1, employer, bigAd.toString(), postedDate, LocalDate.now());
		return j;
	}

	@Override
	public String title() {
		return "Confederation College";
	}

	public static void main(String[] args) throws IOException {
		ConfederationCollegeScraper scraper = new ConfederationCollegeScraper(new CollectionDB());
		Document doc = scraper.loadPage();
		// System.out.println(doc);
		Elements elements = scraper.getElements(doc);

		for (int i = 0; i < elements.size(); i++) {
			Element e = elements.get(i);
			// System.out.println(e);
			// System.out.println("\n\n");
			try {
				scraper.parseAd(e);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}
}
