package unrelated;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TimesWebsites {
	public static void main(String[] args) throws IOException {
		URL url = new URL("http://www.timeswebdesign.com/portfolio");

		Document doc = Jsoup.parse(url, 10000);
		Element table = doc.selectFirst("#node-3 > div > div > div > table > tbody");

		Elements rows = table.select("tr > td > a");

		System.out.println("Rows found: " + rows.size());
		Set<String> addresses = new HashSet<>();

		for (Element row : rows) {
			String attr = row.attr("href");
			if (attr != null && attr.startsWith("http://")) {
				attr = attr.substring(7);
				if (attr.endsWith("/")) {
					attr = attr.substring(0, attr.length() - 1);
				}
				String address = nslookup(attr);
				System.out.println(attr + "," + address);
			}
		}

		System.out.println("Addresses found: " + addresses.size());
		for (String address : addresses) {
			System.out.println(address);
		}
	}

	public static String nslookup(String hostname) throws UnknownHostException {
		InetAddress inetHost = InetAddress.getByName(hostname);
		return inetHost.getHostAddress();
	}
}
